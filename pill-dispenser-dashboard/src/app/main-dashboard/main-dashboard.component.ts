import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { MedicationPlanInterface } from '../models/interfaces/medicationplan';
import { MedicationPlanModel } from '../models/medication-plan.model';
import { MedicationModel } from '../models/medication.model';
import { Popup } from 'ng2-opd-popup';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.css']
})
export class MainDashboardComponent implements OnInit {

  private checkIndex: number;

  showModal: boolean;
  content: string;
  contentIntermed: Array<string> = [];
  title: string;
  time = new Date();
  timer: any;

  constructor(private apiService: ApiService, private medicationPlanModel: MedicationPlanModel, private medicationModel: MedicationModel) { }

  ngOnInit() {
    this.getMedPlans();
    this.timer = setInterval(() => { 
        this.time = new Date();
      }, 1000);
  }

  getMedPlans() {
      this.apiService.getMedPlans().subscribe(
        (data: Array<MedicationPlanInterface>) =>
        {
          this.medicationPlanModel.all = data;
          console.log(this.medicationPlanModel.all[0].medication.length);
          
        }
      );
  }
  //Bootstrap Modal Open event
  show(medicationPlan: any)
  {
    this.contentIntermed = [];
    this.showModal = true; // Show-Hide Modal Check
     for(var i = 0; i < medicationPlan.medication.length; i++) {
       this.contentIntermed[i] = "Starting at " + medicationPlan.startDate + ", you have to take: " 
                              + medicationPlan.medication[i].dosage 
                              + "x " + medicationPlan.medication[i].name + " every " 
                              + medicationPlan.intakeInterval + " hour(s) each day!\n";
     }
    
    //this.content = medicationPlan.medication[0].name; // Dynamic Data
    this.title = "Medication plan " + medicationPlan.id;    // Dynamic Data
    this.content = this.contentIntermed.join("; ");
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }

}
