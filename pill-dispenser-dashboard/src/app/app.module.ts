import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { MedicationModel } from './models/medication.model';
import { MedicationPlanModel } from './models/medication-plan.model';
import { HeaderComponent } from './shared/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { PopupModule } from 'ng2-opd-popup';

@NgModule({
  declarations: [
    AppComponent,
    MainDashboardComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    PopupModule.forRoot()
  ],
  providers: [MedicationModel, MedicationPlanModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
