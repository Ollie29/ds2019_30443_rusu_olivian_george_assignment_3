export interface MedicationInterface {
    name: string;
    dosage: string;
}