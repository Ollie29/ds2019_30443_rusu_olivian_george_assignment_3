import { MedicationInterface } from './medication';

export interface MedicationPlanInterface {
    id: string;
    startDate: string;
    endDate: string;
    intakeInterval: string;
    medication: Array<MedicationInterface>;
}