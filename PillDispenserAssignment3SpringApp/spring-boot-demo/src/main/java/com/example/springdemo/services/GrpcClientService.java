package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.MedicationPlanDTO;
import com.example.springdemo.dto.builder.MedicationBuilder;
import com.pilldispenser.grpc.Medplan;
import com.pilldispenser.grpc.medicationPlanGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GrpcClientService {

    public List<MedicationPlanDTO> receiveMedicationPlan() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();
        //generate stubs
        medicationPlanGrpc.medicationPlanBlockingStub medicationPlanBlockingStub = medicationPlanGrpc.newBlockingStub(channel);
        Medplan.MedPlanRequest request = Medplan.MedPlanRequest.newBuilder().setPatientId(1).build();
        Medplan.MedPlanResponse response = medicationPlanBlockingStub.getMedPlan(request);
        List<MedicationPlanDTO> medicationPlanDTOS = new ArrayList<>();

        for(Medplan.MedPlanResponse.MedicationPlan plan : response.getMedPlanList()) {

            List<MedicationDTO> meds = plan.getMedicationList().stream().map(MedicationBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());

            MedicationPlanDTO dto = new MedicationPlanDTO(plan.getId(), plan.getMedPlanStartDate(), plan.getMedPlanEndDate(), plan.getIntakeInterval(),
                                    meds);
            medicationPlanDTOS.add(dto);
        }
        channel.shutdownNow();
        return medicationPlanDTOS;
    }
}
