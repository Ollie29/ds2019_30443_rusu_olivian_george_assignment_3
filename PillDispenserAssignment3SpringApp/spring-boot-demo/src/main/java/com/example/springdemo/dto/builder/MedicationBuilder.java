package com.example.springdemo.dto.builder;

import com.example.springdemo.dto.MedicationDTO;
import com.pilldispenser.grpc.Medplan;

public class MedicationBuilder {

    public MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medplan.MedPlanResponse.MedicationPlan.Medication medication) {
        return new MedicationDTO(
                medication.getName(),
                medication.getDosage()
        );
    }

}
