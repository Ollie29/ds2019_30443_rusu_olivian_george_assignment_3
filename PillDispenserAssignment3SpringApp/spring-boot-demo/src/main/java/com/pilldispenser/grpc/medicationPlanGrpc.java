package com.pilldispenser.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
        value = "by gRPC proto compiler (version 1.15.0)",
        comments = "Source: medplan.proto")
public final class medicationPlanGrpc {

  private medicationPlanGrpc() {}

  public static final String SERVICE_NAME = "medicationPlan";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.pilldispenser.grpc.Medplan.MedPlanRequest,
          com.pilldispenser.grpc.Medplan.MedPlanResponse> getGetMedPlanMethod;

  @io.grpc.stub.annotations.RpcMethod(
          fullMethodName = SERVICE_NAME + '/' + "getMedPlan",
          requestType = com.pilldispenser.grpc.Medplan.MedPlanRequest.class,
          responseType = com.pilldispenser.grpc.Medplan.MedPlanResponse.class,
          methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.pilldispenser.grpc.Medplan.MedPlanRequest,
          com.pilldispenser.grpc.Medplan.MedPlanResponse> getGetMedPlanMethod() {
    io.grpc.MethodDescriptor<com.pilldispenser.grpc.Medplan.MedPlanRequest, com.pilldispenser.grpc.Medplan.MedPlanResponse> getGetMedPlanMethod;
    if ((getGetMedPlanMethod = medicationPlanGrpc.getGetMedPlanMethod) == null) {
      synchronized (medicationPlanGrpc.class) {
        if ((getGetMedPlanMethod = medicationPlanGrpc.getGetMedPlanMethod) == null) {
          medicationPlanGrpc.getGetMedPlanMethod = getGetMedPlanMethod =
                  io.grpc.MethodDescriptor.<com.pilldispenser.grpc.Medplan.MedPlanRequest, com.pilldispenser.grpc.Medplan.MedPlanResponse>newBuilder()
                          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
                          .setFullMethodName(generateFullMethodName(
                                  "medicationPlan", "getMedPlan"))
                          .setSampledToLocalTracing(true)
                          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                  com.pilldispenser.grpc.Medplan.MedPlanRequest.getDefaultInstance()))
                          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                                  com.pilldispenser.grpc.Medplan.MedPlanResponse.getDefaultInstance()))
                          .setSchemaDescriptor(new medicationPlanMethodDescriptorSupplier("getMedPlan"))
                          .build();
        }
      }
    }
    return getGetMedPlanMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medicationPlanStub newStub(io.grpc.Channel channel) {
    return new medicationPlanStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medicationPlanBlockingStub newBlockingStub(
          io.grpc.Channel channel) {
    return new medicationPlanBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medicationPlanFutureStub newFutureStub(
          io.grpc.Channel channel) {
    return new medicationPlanFutureStub(channel);
  }

  /**
   */
  public static abstract class medicationPlanImplBase implements io.grpc.BindableService {

    /**
     */
    public void getMedPlan(com.pilldispenser.grpc.Medplan.MedPlanRequest request,
                           io.grpc.stub.StreamObserver<com.pilldispenser.grpc.Medplan.MedPlanResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetMedPlanMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
              .addMethod(
                      getGetMedPlanMethod(),
                      asyncUnaryCall(
                              new MethodHandlers<
                                      com.pilldispenser.grpc.Medplan.MedPlanRequest,
                                      com.pilldispenser.grpc.Medplan.MedPlanResponse>(
                                      this, METHODID_GET_MED_PLAN)))
              .build();
    }
  }

  /**
   */
  public static final class medicationPlanStub extends io.grpc.stub.AbstractStub<medicationPlanStub> {
    private medicationPlanStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlanStub(io.grpc.Channel channel,
                               io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlanStub build(io.grpc.Channel channel,
                                       io.grpc.CallOptions callOptions) {
      return new medicationPlanStub(channel, callOptions);
    }

    /**
     */
    public void getMedPlan(com.pilldispenser.grpc.Medplan.MedPlanRequest request,
                           io.grpc.stub.StreamObserver<com.pilldispenser.grpc.Medplan.MedPlanResponse> responseObserver) {
      asyncUnaryCall(
              getChannel().newCall(getGetMedPlanMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medicationPlanBlockingStub extends io.grpc.stub.AbstractStub<medicationPlanBlockingStub> {
    private medicationPlanBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlanBlockingStub(io.grpc.Channel channel,
                                       io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlanBlockingStub build(io.grpc.Channel channel,
                                               io.grpc.CallOptions callOptions) {
      return new medicationPlanBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.pilldispenser.grpc.Medplan.MedPlanResponse getMedPlan(com.pilldispenser.grpc.Medplan.MedPlanRequest request) {
      return blockingUnaryCall(
              getChannel(), getGetMedPlanMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medicationPlanFutureStub extends io.grpc.stub.AbstractStub<medicationPlanFutureStub> {
    private medicationPlanFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlanFutureStub(io.grpc.Channel channel,
                                     io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlanFutureStub build(io.grpc.Channel channel,
                                             io.grpc.CallOptions callOptions) {
      return new medicationPlanFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.pilldispenser.grpc.Medplan.MedPlanResponse> getMedPlan(
            com.pilldispenser.grpc.Medplan.MedPlanRequest request) {
      return futureUnaryCall(
              getChannel().newCall(getGetMedPlanMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_MED_PLAN = 0;

  private static final class MethodHandlers<Req, Resp> implements
          io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
          io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
          io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
          io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medicationPlanImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medicationPlanImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_MED_PLAN:
          serviceImpl.getMedPlan((com.pilldispenser.grpc.Medplan.MedPlanRequest) request,
                  (io.grpc.stub.StreamObserver<com.pilldispenser.grpc.Medplan.MedPlanResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
            io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medicationPlanBaseDescriptorSupplier
          implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medicationPlanBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.pilldispenser.grpc.Medplan.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medicationPlan");
    }
  }

  private static final class medicationPlanFileDescriptorSupplier
          extends medicationPlanBaseDescriptorSupplier {
    medicationPlanFileDescriptorSupplier() {}
  }

  private static final class medicationPlanMethodDescriptorSupplier
          extends medicationPlanBaseDescriptorSupplier
          implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medicationPlanMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medicationPlanGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
                  .setSchemaDescriptor(new medicationPlanFileDescriptorSupplier())
                  .addMethod(getGetMedPlanMethod())
                  .build();
        }
      }
    }
    return result;
  }
}
